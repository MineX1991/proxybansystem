package de.MineX1991.ProxyBanSystem.Manager;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

/**
 * @author MineX1991
 */
public class FileManager {
 
  public static Configuration BanConfig;
  public static File BanFile;
  public static Configuration MuteConfig;
  public static File MuteFile;
  public static Configuration MessagesConfiguration;
  public static File MsgFile;
  
  public static void setDefaultConfig() {
      if (BanConfig.get("BannedPlayers") == null) {
          try {
              BanConfig.set("BannedPlayers", null);
              ConfigurationProvider.getProvider(YamlConfiguration.class).save(BanConfig, BanFile);
          } catch (IOException ex) {
              ex.printStackTrace();
          }
      }
  }
  public static void saveBanFile() {
      try {
          ConfigurationProvider.getProvider(YamlConfiguration.class).save(BanConfig, BanFile);
      } catch (IOException ex) {
          ex.printStackTrace();
      }
  }
  public static void saveMuteFile() {
      try {
          ConfigurationProvider.getProvider(YamlConfiguration.class).save(MuteConfig, MuteFile);
      } catch (IOException ex) {
          ex.printStackTrace();
      }
  }
  public static void saveMessagesConfiguration() {
      try {
          ConfigurationProvider.getProvider(YamlConfiguration.class).save(MessagesConfiguration, MsgFile);
      } catch (Exception localException) {}
  }
}