package de.MineX1991.ProxyBanSystem.Listener;

import de.MineX1991.ProxyBanSystem.API.BanManager;
import de.MineX1991.ProxyBanSystem.API.MuteManager;
import de.MineX1991.ProxyBanSystem.Main.Main;
import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.LoginEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.event.EventHandler;

public class CListener
  implements Listener
{
  public static ArrayList<ProxiedPlayer> list = new ArrayList();
  
    @EventHandler
  public void onLogin(LoginEvent e)
  {
    String playername = e.getConnection().getName();
    BanManager.createPlayer(playername);
    MuteManager.createPlayer(playername);
    if (BanManager.isBanned(playername))
    {
      long current = System.currentTimeMillis();
      long end = BanManager.getEnd(playername);
      if ((end > current) || (end == -1L))
      {
        e.setCancelled(true);
        e.setCancelReason(BanManager.getBannedMessage(playername));
      }
      else
      {
        e.setCancelled(false);
        BanManager.unBan(playername, "Automatische Cloud");
      }
    }
  }

  @EventHandler
  public void onChat(ChatEvent e)
  {
    ProxiedPlayer ps = (ProxiedPlayer)e.getSender();
    final ProxiedPlayer p = (ProxiedPlayer)e.getSender();
    if ((!e.getMessage().startsWith("/")) && (!p.hasPermission("proxybansystem.anti.spam"))) {
      if (list.contains(p))
      {
        e.setCancelled(true);
        p.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "§cBitte warte einen Moment... Verdacht auf Spam!");
      }
      else
      {
        list.add(p);
        ProxyServer.getInstance().getScheduler().schedule(Main.getProxyBan(), new Runnable()
        {
          public void run()
          {
            CListener.list.remove(p);
          }
        }, 3L, TimeUnit.SECONDS);
      }
    }
    String msg2 = e.getMessage();
    if (MuteManager.isMuted(p.getName()))
    {
      long current = System.currentTimeMillis();
      long end = MuteManager.getEnd(p.getName());
      if ((end < current) && (end != -1L))
      {
        MuteManager.unMute(p.getName(), "Automatische Cloud");
        e.setCancelled(false);
      }
      else if (!e.getMessage().startsWith("/"))
      {
        e.setCancelled(true);
        p.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "§7Derzeit ist der Chat für dich Deaktiviert!");
        p.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "§7Grund: §c" + MuteManager.getReason(p.getName()));
        p.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "§7Verbleibende Zeit: §c" + MuteManager.getRemainingTime(p.getName()));
      }
    }
  }
}
