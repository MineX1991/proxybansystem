package de.MineX1991.ProxyBanSystem.Main;

import de.MineX1991.ProxyBanSystem.Commands.BanList;
import de.MineX1991.ProxyBanSystem.Commands.Check;
import de.MineX1991.ProxyBanSystem.Commands.Kick;
import de.MineX1991.ProxyBanSystem.Commands.MuteList;
import de.MineX1991.ProxyBanSystem.Commands.Strafe;
import de.MineX1991.ProxyBanSystem.Commands.TempBan;
import de.MineX1991.ProxyBanSystem.Commands.TempMute;
import de.MineX1991.ProxyBanSystem.Commands.UnBan;
import de.MineX1991.ProxyBanSystem.Commands.UnMute;
import de.MineX1991.ProxyBanSystem.Listener.CListener;
import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import de.MineX1991.ProxyBanSystem.Manager.FileManager;
import java.io.File;
import java.io.IOException;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;

/**
 * @author MineX1991
 */
public class Main extends Plugin {
    
    private static Main Instance;
    
    @Override
    public void onEnable() {
        Instance = this;
        ConfigManager.loadConfiguration();
        createFolders();
        createFile();
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new Strafe());
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new TempBan());
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new TempMute());
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new UnMute());
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new UnBan());
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new Check());
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new Kick());
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new MuteList());
        BungeeCord.getInstance().getPluginManager().registerCommand(this, new BanList());
        BungeeCord.getInstance().getPluginManager().registerListener(this, new CListener());
        BungeeCord.getInstance().getConsole().sendMessage("§l");
        BungeeCord.getInstance().getConsole().sendMessage("§9---------- §3ProxyBanSystem §9----------");
        BungeeCord.getInstance().getConsole().sendMessage("§l ");
        BungeeCord.getInstance().getConsole().sendMessage("§eAKTIVIERT");
        BungeeCord.getInstance().getConsole().sendMessage("§7Version: §e" + Main.Instance.getDescription().getVersion());
        BungeeCord.getInstance().getConsole().sendMessage("§7TeamSpeak: §eevil-company.com");
        BungeeCord.getInstance().getConsole().sendMessage("§7YouTube: §eBungeeDev");
        BungeeCord.getInstance().getConsole().sendMessage("§7Website: §ewww.evil-company.com");
        BungeeCord.getInstance().getConsole().sendMessage("§7Skype: §eSeelenfresser8");
        BungeeCord.getInstance().getConsole().sendMessage("§7Minecraft-Name §eTheBig_Brother");
        BungeeCord.getInstance().getConsole().sendMessage("§9---------------------------------------");
    }
  public void createFile() {
    if (!getDataFolder().exists()) {
      getDataFolder().mkdir();
    }
  }
    public void createFolders() {
    if (!getDataFolder().exists()) {
      getDataFolder().mkdir();
    }
    if (!(FileManager.BanFile = new File(getDataFolder().getPath(), "bans.yml")).exists()) {
      try {
        FileManager.BanFile.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    if (!(FileManager.MuteFile = new File(getDataFolder().getPath(), "mutes.yml")).exists()) {
      try {
        FileManager.MuteFile.createNewFile();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    try {
      FileManager.MuteConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(FileManager.MuteFile);
    } catch (IOException e1) {
      e1.printStackTrace();
    }
    try {
      FileManager.BanConfig = ConfigurationProvider.getProvider(YamlConfiguration.class).load(FileManager.BanFile);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
    @Override
    public void onDisable() {
        BungeeCord.getInstance().getConsole().sendMessage("§l");
        BungeeCord.getInstance().getConsole().sendMessage("§9---------- §3ProxyBanSystem §9----------");
        BungeeCord.getInstance().getConsole().sendMessage("§l ");
        BungeeCord.getInstance().getConsole().sendMessage("§4DEAKTIVIERT!");
        BungeeCord.getInstance().getConsole().sendMessage("§7Version: §e" + Main.Instance.getDescription().getVersion());
        BungeeCord.getInstance().getConsole().sendMessage("§7TeamSpeak: §eevil-company.com");
        BungeeCord.getInstance().getConsole().sendMessage("§7YouTube: §eBungeeDev");
        BungeeCord.getInstance().getConsole().sendMessage("§7Website: §ewww.evil-company.com");
        BungeeCord.getInstance().getConsole().sendMessage("§7Skype: §eSeelenfresser8");
        BungeeCord.getInstance().getConsole().sendMessage("§7Minecraft-Name §eTheBig_Brother");
        BungeeCord.getInstance().getConsole().sendMessage("§9---------------------------------------");
    }
    public static Main getProxyBan() {
        return Instance;
    }
}