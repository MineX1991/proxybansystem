package de.MineX1991.ProxyBanSystem.API;

import de.MineX1991.ProxyBanSystem.Manager.FileManager;
import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;

/**
 * @author MineX1991
 */
public class MuteManager {

    static Configuration cfg = FileManager.MuteConfig;
    
    public static boolean exists(String playername) {
        if (cfg.get("Players." + PlayerAPI.getUUID(playername)) != null) {
            return true;
        }
        return false;
    }
    public static void createPlayer(String playername) {
        if (!exists(playername)) {
            cfg.set("Players." + PlayerAPI.getUUID(playername) + ".Playername", playername);
            cfg.set("Players." + PlayerAPI.getUUID(playername) + ".Muted", Boolean.valueOf(false));
            cfg.set("Players." + PlayerAPI.getUUID(playername) + ".Reason", "");
            cfg.set("Players." + PlayerAPI.getUUID(playername) + ".By", "");
            cfg.set("Players." + PlayerAPI.getUUID(playername) + ".End", Integer.valueOf(0));
            FileManager.saveMuteFile();
        }
    }
    public static boolean isMuted(String playername) {
        if (exists(playername)) {
            return cfg.getBoolean("Players." + PlayerAPI.getUUID(playername) + ".Muted");
        }
        return false;
    }
    public static void Mute(String playername, String Reason, String von, int seconds) {
        if (!isMuted(playername)) {
            
        long current = System.currentTimeMillis();
        long end = current + seconds * 1000;
        if (seconds == -1) {
          end = -1L;
        }
        cfg.set("Players." + PlayerAPI.getUUID(playername) + ".Playername", playername);
        cfg.set("Players." + PlayerAPI.getUUID(playername) + ".Muted", Boolean.valueOf(true));
        cfg.set("Players." + PlayerAPI.getUUID(playername) + ".Reason", Reason);
        cfg.set("Players." + PlayerAPI.getUUID(playername) + ".By", von);
        cfg.set("Players." + PlayerAPI.getUUID(playername) + ".End", Long.valueOf(end));
        FileManager.saveBanFile();
        ArrayList<String> muted = cfg.getStringList("MutedPlayers") != null ? (ArrayList)cfg.getStringList("MutedPlayers") : new ArrayList();
        muted.add(playername);
        cfg.set("MutedPlayers", muted);
        FileManager.saveMuteFile();
        }
    }
    public static void unMute(String playername, String By) {
    if (isMuted(playername)) {
      cfg.set("Players." + PlayerAPI.getUUID(playername) + ".Playername", playername);
      cfg.set("Players." + PlayerAPI.getUUID(playername) + ".Muted", Boolean.valueOf(false));
      cfg.set("Players." + PlayerAPI.getUUID(playername) + ".Reason", "");
      cfg.set("Players." + PlayerAPI.getUUID(playername) + ".By", "");
      cfg.set("Players." + PlayerAPI.getUUID(playername) + ".End", Integer.valueOf(0));
      FileManager.saveBanFile();
      List banned = cfg.getStringList("MutedPlayers");
      banned.remove(playername);
      cfg.set("MutedPlayers", banned);
      FileManager.saveMuteFile();
      for (ProxiedPlayer o : BungeeCord.getInstance().getPlayers()) {
        if (o.hasPermission("proxybansystem.unmute.see")) {
          o.sendMessage(String.valueOf("Der Spieler §c" + playername + " §7wurde von §c" + By + " §7entmuted."));
        }
      }
    }
  }
    public static List<String> getMutedPlayers() {
    return cfg.getStringList("MutedPlayers");
  }
    public static String getReason(String playername) {
    String reason = "";
    if (isMuted(playername)) {
      reason = cfg.getString("Players." + PlayerAPI.getUUID(playername) + ".Reason");
    }
    return reason;
  }
    public static String getWhoMuted(String playername) {
    String whomuted = "";
    if (isMuted(playername)) {
      whomuted = cfg.getString("Players." + PlayerAPI.getUUID(playername) + ".By");
    }
    return whomuted;
  }
    public static long getEnd(String playername) {
    long end = -1L;
    if (isMuted(playername)) {
      end = cfg.getLong("Players." + PlayerAPI.getUUID(playername) + ".End");
    }
    return end;
  }
    public static String getRemainingTime(String playername) {
    String remainingTime = "";
    if (isMuted(playername)) {
      long current = System.currentTimeMillis();
      long end = getEnd(playername);
      long difference = end - current;
      if (end == -1L) {
        return "§4Permanent";
      }
      int Sekunden = 0;
      int Minuten = 0;
      int Stunden = 0;
      int Tage = 0;
      while (difference >= 1000L) {
        difference -= 1000L;
        Sekunden++;
      } while (Sekunden >= 60) {
        Sekunden -= 60;
        Minuten++;
      } while (Minuten >= 60) {
        Minuten -= 60;
        Stunden++;
      } while (Stunden >= 24) {
        Stunden -= 24;
        Tage++;
      }
      remainingTime = "§b" + Tage + " Tag(e), " + Stunden + " Stunde(n), " + Minuten + " Minute(n) " + Sekunden + " Sekunden";
    }
    return remainingTime;
  }
}