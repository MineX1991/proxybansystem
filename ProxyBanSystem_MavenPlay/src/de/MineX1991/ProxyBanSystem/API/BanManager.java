package de.MineX1991.ProxyBanSystem.API;

import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import de.MineX1991.ProxyBanSystem.Manager.FileManager;
import java.util.ArrayList;
import java.util.List;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.config.Configuration;

/**
 * @author MineX1991
 */
public class BanManager {

  static Configuration cfg = FileManager.BanConfig;
  static Configuration cfgi = FileManager.BanConfig;
  
  public static boolean exists(String playername) {
      if (cfg.get("Spieler." + PlayerAPI.getUUID(playername)) != null) {
          return true;
      }
      return false;
  }
  public static void createPlayer(String Spielername) {
      if (!exists(Spielername)) {
          cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Spielername", Spielername);
          cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Ban", Boolean.valueOf(false));
          cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Grund", "");
          cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".von", "");
          cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Ende", Integer.valueOf(0));
          FileManager.saveBanFile();
      }
  }
  public static boolean isBanned(String Spielername) {
      if (exists(Spielername)) {
          return cfg.getBoolean("Spieler." + PlayerAPI.getUUID(Spielername) + ".Ban");
      }
      return false;
  }
  public static void Ban(String Spielername, String Grund, String von, int Sekunden) {
      if (!isBanned(Spielername)) {
        long current = System.currentTimeMillis();
        long end = current + Sekunden * 1000;
      if (Sekunden == -1) {
            end = -1L;
         }
      cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Spielername", Spielername);
      cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Ban", Boolean.valueOf(true));
      cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Grund", Grund);
      cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".von", von);
      cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Ende", Long.valueOf(end));
      FileManager.saveBanFile();
      ProxiedPlayer target = BungeeCord.getInstance().getPlayer(Spielername);
      if (target != null) {
          target.disconnect(getBannedMessage(Spielername));
      }
      ArrayList<String> banned = cfg.getStringList("GebannteSpieler") != null ? (ArrayList)cfg.getStringList("GebannteSpieler") : new ArrayList();
      banned.add(Spielername);
      cfg.set("GebannteSpieler", banned);
      FileManager.saveBanFile();
      }
  }
  public static void unBan(String Spielername, String von) {
      if (isBanned(Spielername)) {
        cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Spielername", Spielername);
        cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Ban", Boolean.valueOf(false));
        cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Grund", "");
        cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".von", "");
        cfg.set("Spieler." + PlayerAPI.getUUID(Spielername) + ".Ende", Integer.valueOf(0));
        FileManager.saveBanFile();
        List Ban2 = cfg.getStringList("GebannteSpieler");
        Ban2.remove(Spielername);
        cfg.set("GebannteSpieler", Ban2);
        FileManager.saveBanFile();
        for (ProxiedPlayer o : BungeeCord.getInstance().getPlayers()) {
          if (o.hasPermission("proxybansystem.unban.see")) {
              o.sendMessage(String.valueOf("§7Der Spieler §b" + Spielername + " §7wurde von §c" + von + " §7entbannt."));
          }
      }
      }
  }
  public static List<String> getBannedPlayers() {
      return cfg.getStringList("GebannteSpieler");
  }
  public static String getReason(String Spielername) {
    String Grund = "";
    if (isBanned(Spielername)) {
        Grund = cfg.getString("Spieler." + PlayerAPI.getUUID(Spielername) + ".Grund");
    }
    return Grund;
  }
  public static String getWhoBanned(String Spielername) {
    String whobanned = "";
    if (isBanned(Spielername)) {
        whobanned = cfg.getString("Spieler." + PlayerAPI.getUUID(Spielername) + ".von");
    }
    return whobanned;
  }
  public static void addtoList(String Spielername, String Grund) {
    cfgi.set("BereitsGebannt." + PlayerAPI.getUUID(Spielername), "true");
    cfgi.set("BanGrund." + PlayerAPI.getUUID(Spielername), Grund);
  }
  public static boolean getfromlist(String Spielername) {
    boolean bool = false;
    if (cfgi.equals("BereitsGebannt." + PlayerAPI.getUUID(Spielername))) {
      if (cfgi.get("BereitsGebannt." + PlayerAPI.getUUID(Spielername)).equals("true")) {
        bool = true;
      }
    } else {
      addtoList(Spielername, "Grundi");
      bool = false;
    }
    return bool;
  }
  public static String getReasonfromlast(String Spielername) {
    String bool = "Unbekannt";
    bool = cfgi.getString("BanGrund." + PlayerAPI.getUUID(Spielername));
    return bool;
  }
  public static long getEnd(String Spielername) {
      long end = -1L;
    if (isBanned(Spielername)) {
      end = cfg.getLong("Spieler." + PlayerAPI.getUUID(Spielername) + ".Ende");
    }
    return end;
  }
  public static String getRemainingTime(String Spielername) {
    String remainingTime = "";
    if (isBanned(Spielername)) {
      long current = System.currentTimeMillis();
      long end = getEnd(Spielername);
      long difference = end - current;
      if (end == -1L) {
          return "§4Permanent";
      }
      int Sekunden = 0;
      int Minuten = 0;
      int Stunden = 0;
      int Tage = 0;
      while (difference >= 1000L) {
        difference -= 1000L;
        Sekunden++;
      } while (Sekunden >= 60) {
          Sekunden -= 60;
          Minuten++;
      } while (Minuten >= 60) {
          Minuten -= 60;
          Stunden++;
      } while (Stunden >= 24) {
          Stunden -= 24;
          Tage++;
      }
      remainingTime = "§b" + Tage + " Tag(e), " + Stunden + " Stunde(n), " + Minuten + " Minute(n) " + Sekunden + " Sekunde (n)";
    }
    return remainingTime;
  }
  public static String getBannedMessage(String Spielername) {
      String BanMsg = "";
      if (isBanned(Spielername)) {
      BanMsg = "§7Du wurdest vom\n§6" + ConfigManager.getNetworkName() + " Netzwerk gebannt!\n\n§4GRUND:\n" + getReason(Spielername) +
              "\nVerbleibene Zeit:\n" + getRemainingTime(Spielername) +
              "\n\n§bZu unrechter Ban?\n§9Dann stelle einfach auf unserem Forum ein Entbannungsantrag\n§aFORUM: " + ConfigManager.getForum();
      }
      return BanMsg;
  }
}