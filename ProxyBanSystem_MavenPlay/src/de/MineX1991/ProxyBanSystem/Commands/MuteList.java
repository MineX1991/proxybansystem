package de.MineX1991.ProxyBanSystem.Commands;

import de.MineX1991.ProxyBanSystem.API.MuteManager;
import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class MuteList
extends Command {
    public MuteList() {
        super(ConfigManager.getMutelistCommand());
    }

    public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission("proxybansystem.mutelist")) {
            if (MuteManager.getMutedPlayers().size() == 0) {
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Es sind derzeit keine \u00a7cSpieler \u00a77gemuted.");
                return;
            }
            sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Diese \u00a7cSpieler \u00a77sind derzeit Gemuted:");
            for (String x : MuteManager.getMutedPlayers()) {
                sender.sendMessage("\u00a7c" + x + " \u00a78| \u00a77/check " + x);
            }
        } else {
            sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + ConfigManager.getNoPerm());
        }
    }
}

