package de.MineX1991.ProxyBanSystem.Commands;

import de.MineX1991.ProxyBanSystem.API.BanManager;
import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class UnBan
extends Command {
    public UnBan() {
        super(ConfigManager.getUnbanCommand());
    }

    public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission("bungeecord.command.unban")) {
            if (args.length != 1) {
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Ausf\u00fchrung:\u00a7c /unban <Spieler>");
            } else {
                if (!BanManager.isBanned(args[0])) {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Dieser \u00a7cSpieler \u00a77ist nicht gebannt.");
                    return;
                }
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Der \u00a7cSpieler \u00a77wurde entbannt.");
                BanManager.unBan(args[0], sender.getName());
            }
        } else {
            sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + ConfigManager.getNoPerm());
        }
    }
}