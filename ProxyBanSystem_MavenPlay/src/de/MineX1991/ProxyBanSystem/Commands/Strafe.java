package de.MineX1991.ProxyBanSystem.Commands;

import de.MineX1991.ProxyBanSystem.API.BanManager;
import de.MineX1991.ProxyBanSystem.API.MuteManager;
import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import java.util.Random;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Strafe
extends Command {
	public Strafe() {
		super(ConfigManager.getBanCommand());
	}

	@SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
		if (sender.hasPermission("proxybansystem.ban")) {
			if (args.length != 2) {
				sender.sendMessage(ConfigManager.getPrefix() + "§bWarum Willst du den Strafen?");
				sender.sendMessage(ConfigManager.getPrefix()  + "§4§lHack Strafen");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Hacks §7| §cAntiKnockback : §b1");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Hacks §7| §cX-Ray : §b2");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Hacks §7| §cKillAura : §b3");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Hacks §7| §cFly : §b4");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Hacks §7| §cSpeed : §b5");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Hacks §7| §cSonstiges : §b6");
				sender.sendMessage(ConfigManager.getPrefix()  + "§a§lBeleidigungen Strafen");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Beleidigungen §7| §cTeam : §b7");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Beleidigungen §7| §cSpieler : §b8");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Beleidigungen §7| §cNetzwerk : §b9");
				sender.sendMessage(ConfigManager.getPrefix()  + "§e§lSonstige Strafen");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Sonstiges §7| §cBugusing : §b10");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Sonstiges §7| §cDrohung : §b11");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Sonstiges §7| §cRadikalismus : §b12");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Sonstiges §7| §cWerbung : §b13");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Sonstiges §7| §cRechte Ausnutzung : §b14");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Sonstiges §7| §cGriefing : §b15");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Sonstiges §7| §cCombat-Log : §b16");
				sender.sendMessage(ConfigManager.getPrefix()  + "§3§lWeitere Strafen");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Weiteres §7| §cTeaming : §b17");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Weiteres §7| §cLeichte Beleidung : §b18");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Weiteres §7| §cRespektloses Verhalten: §b19");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Weiteres §7| §cReport-Abuse: §b20");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Weiteres §7| §cProvokation-Beleidigung: §b21");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Weiteres §7| §cProvokation-Allgemein: §b22");
				sender.sendMessage(ConfigManager.getPrefix()  + "§7§ §8Weiteres §7| §cBan-Umgehung: §b23");


				return;
			} else {
				String bannummer = args[1];
				String banname = args[0];
				if (BanManager.isBanned(banname)) {
					sender.sendMessage(ConfigManager.getPrefix()
							+ "§cDieser Spieler wurde bereits bestraft. Seine Strafe wird nun überschrieben...");
				}

				sender.sendMessage(ConfigManager.getPrefix() + "§7Du hast den Spieler erfolgreich bestraft.");
				

				switch (bannummer) {
				case "1":
					BanManager.Ban(banname, "Hacking Anti-Knockback #" + getBanID(),
					sender.getName(), 16 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Hacking Anti-Knockback"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "2":
					BanManager.Ban(banname, "Hacking X-Ray #" + getBanID(),
					sender.getName(), 20 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Hacking X-Ray"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "3":
					BanManager.Ban(banname, "Hacking KillAura #" + getBanID(),
					sender.getName(), 20 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Hacking KillAura"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "4":
					BanManager.Ban(banname, "Hacking Fliegen/Fly #" + getBanID(),
					sender.getName(), 14 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Hacking Fliegen/Fly"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "5":
					BanManager.Ban(banname, "Hacking Speed #" + getBanID(),
					sender.getName(), 12 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Hacking Speed"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "6":
					BanManager.Ban(banname, "Hacking Sonstiges #" + getBanID(),
					sender.getName(), 21 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Hacking Sonstiges"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "7":
					MuteManager.Mute(banname, "Beleidigungen Teamler #" + getBanID(),
							sender.getName(), 3 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
								if(all.hasPermission("proxybansystem.ban.see")) {
									all.sendMessage(new TextComponent("§8---------- §4MuteMeldung §8----------"));
									all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
									all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
									all.sendMessage(new TextComponent("§8Grund: §3Beleidigungen Teamler"));
									all.sendMessage(new TextComponent("§8Dauer: §3" + MuteManager.getRemainingTime(banname)));
									all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
									all.sendMessage(new TextComponent("§8-------------------------------------"));
								}
							}
					break;
				case "8":
					MuteManager.Mute(banname, "Beleidigungen Spieler #" + getBanID(),
					sender.getName(), 2 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4MuteMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Beleidigungen Spieler"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + MuteManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8-------------------------------------"));
						}
					}
					break;
				case "9":
					MuteManager.Mute(banname, "Beleidigungen Netzwerk #" + getBanID(),
					sender.getName(), 1 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4MuteMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Beleidigungen Netzwerk"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + MuteManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8-------------------------------------"));
						}
					}
					break;
				case "10":
					BanManager.Ban(banname, "Fehlverhalten Bugusing #" + getBanID(),
							sender.getName(),  60 * 60 * 12);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Fehlverhalten Bugusing"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "11":
					BanManager.Ban(banname, "Fehlverhalten Drohung #" + getBanID(),
							sender.getName(),  -1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Fehlverhalten Drohung"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "12":
					BanManager.Ban(banname, "Fehlverhalten Radikalismus #" + getBanID(),
					sender.getName(),  -1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Fehlverhalten Radikalismus"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "13":
					BanManager.Ban(banname, "Fehlverhalten Werbung #" + getBanID(),
					sender.getName(), -1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Fehlverhalten Werbung"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "14":
					BanManager.Ban(banname, "Rechte Ausnutzung #" + getBanID(),
					sender.getName(),  14 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Rechte Ausnutzung"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "15":
					BanManager.Ban(banname, "Griefing #" + getBanID(),
					sender.getName(), -1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Griefing"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "16":
					BanManager.Ban(banname, "Combat-Log #" + getBanID(),
					sender.getName(), 7 * 60 * 60 * 24);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Combat-Log"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "17":
					BanManager.Ban(banname, "Teaming #" + getBanID(),
					sender.getName(), 1 * 60 * 60 * 1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Teaming"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "18":
					MuteManager.Mute(banname, "Leichte Beleidung #" + getBanID(),
					sender.getName(),  60 * 60 * 1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4MuteMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Leichte Beleidung"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + MuteManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8-------------------------------------"));
						}
					}
					break;
				case "19":
					MuteManager.Mute(banname, "Respektloses Verhalten #" + getBanID(),
					sender.getName(), 3 * 60 * 60 * 1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4MuteMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Respektloses Verhalten"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + MuteManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8-------------------------------------"));
						}
					}
					break;
				case "20":
					BanManager.Ban(banname, "Report-Abuse #" + getBanID(),
					sender.getName(), 1 * 60 * 60 * 1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Report-Abuse"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "21":
					BanManager.Ban(banname, "Provokation-Beleidigung #" + getBanID(),
					sender.getName(), 5 * 60 * 60 * 1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Provokation-Beleidigung"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "22":
					BanManager.Ban(banname, "Provokation-Allgemein #" + getBanID(),
					sender.getName(), 5 * 60 * 60 * 1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Provokation-Allgemein"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				case "23":
					BanManager.Ban(banname, "Ban-Umgehung #" + getBanID(),
					sender.getName(), -1);
					for(ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
						if(all.hasPermission("proxybansystem.ban.see")) {
							all.sendMessage(new TextComponent("§8---------- §4BanMeldung §8----------"));
							all.sendMessage(new TextComponent("§8Spieler: §3" + banname));
							all.sendMessage(new TextComponent("§8BanID: §3" + getBanID()));
							all.sendMessage(new TextComponent("§8Grund: §3Ban-Umgehung"));
							all.sendMessage(new TextComponent("§8Dauer: §3" + BanManager.getRemainingTime(banname)));
							all.sendMessage(new TextComponent("§8Ban Ersteller: §e" + sender.getName()));
							all.sendMessage(new TextComponent("§8------------------------------------"));
						}
					}
					break;
				}
			}
		} else {
			sender.sendMessage(ConfigManager.getPrefix() + ConfigManager.getNoPerm());
		}
	}
	
	
	public String getBanID() {
		String str = "";
		int lastrandom = 0;
		for(int i = 0; i < 4; i ++) {
			Random r = new Random();
			int rand = r.nextInt(9);
			while(rand == lastrandom) {
				rand = r.nextInt(9);
			}
			lastrandom = rand;
			str = str + rand;
		}
		
		return str;
	}

}


