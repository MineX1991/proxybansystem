package de.MineX1991.ProxyBanSystem.Commands;

import de.MineX1991.ProxyBanSystem.API.BanManager;
import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class TempBan
extends Command {
    public TempBan() {
        super(ConfigManager.getTempbanCommand());
    }

    public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission("proxybansystem.tempban")) {
            if (args.length < 4) {
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Ausfürung:\u00a7c /Tempban <Spieler> <Zeit> <Zeitform> <Grund> ");
            } else {
                if (BanManager.isBanned(args[0])) {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Dieser \u00a7cSpieler \u00a77ist bereits gebannt.");
                    return;
                }
                String TimeUnit2 = args[2];
                String message = "";
                int i = 3;
                while (i < args.length) {
                    message = String.valueOf(message) + args[i] + " ";
                    ++i;
                }
                int Time = Integer.parseInt(args[1]);
                if (TimeUnit2.equalsIgnoreCase("sec") || TimeUnit2.equalsIgnoreCase("s") || TimeUnit2.equalsIgnoreCase("second") || TimeUnit2.equalsIgnoreCase("seconds") || TimeUnit2.equalsIgnoreCase("secs")) {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Der Spieler \u00a7c" + args[0] + "\u00a77 wurde erfolreich gebannt.");
                    BanManager.Ban(args[0], message, sender.getName(), Time * 1);
                } else if (TimeUnit2.equalsIgnoreCase("min") || TimeUnit2.equalsIgnoreCase("minute") || TimeUnit2.equalsIgnoreCase("m") || TimeUnit2.equalsIgnoreCase("mins") || TimeUnit2.equalsIgnoreCase("minutes")) {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Der Spieler \u00a7c" + args[0] + "\u00a77 wurde erfolreich gebannt.");
                    BanManager.Ban(args[0], message, sender.getName(), Time * 60);
                } else if (TimeUnit2.equalsIgnoreCase("h") || TimeUnit2.equalsIgnoreCase("hour") || TimeUnit2.equalsIgnoreCase("hours")) {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Der Spieler \u00a7c" + args[0] + "\u00a77 wurde erfolreich gebannt.");
                    BanManager.Ban(args[0], message, sender.getName(), Time * 60 * 60);
                } else if (TimeUnit2.equalsIgnoreCase("d") || TimeUnit2.equalsIgnoreCase("day") || TimeUnit2.equalsIgnoreCase("days")) {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Der Spieler \u00a7c" + args[0] + "\u00a77 wurde erfolreich gebannt.");
                    BanManager.Ban(args[0], message, sender.getName(), Time * 60 * 60 * 24);
                } else if (TimeUnit2.equalsIgnoreCase("w") || TimeUnit2.equalsIgnoreCase("week") || TimeUnit2.equalsIgnoreCase("weeks")) {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Der Spieler \u00a7c" + args[0] + "\u00a77 wurde erfolreich gebannt.");
                    BanManager.Ban(args[0], message, sender.getName(), Time * 60 * 60 * 24 * 7);
                } else {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Verwende folgende Zeitformen");
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a7c< s | m | h | d | w >");
                }
            }
        } else {
            sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + ConfigManager.getNoPerm());
        }
    }
}

