package de.MineX1991.ProxyBanSystem.Commands;
//
import de.MineX1991.ProxyBanSystem.API.MuteManager;
import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class UnMute
extends Command {
    public UnMute() {
        super(ConfigManager.getUnmuteCommand());
    }

    public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission("proxybansystem.unmute")) {
            if (args.length < 1) {
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Ausf\u00fchrung:\u00a7c /unmute <Spieler>");
            } else {
                if (!MuteManager.isMuted(args[0])) {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Dieser \u00a7cSpieler\u00a77 ist nicht gemuted.");
                    return;
                }
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Dieser \u00a7cSpieler \u00a77wurde erfolgreich entmuted.");
                MuteManager.unMute(args[0], sender.getName());
            }
        } else {
            sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + ConfigManager.getNoPerm());
        }
    }
}

