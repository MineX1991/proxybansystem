package de.MineX1991.ProxyBanSystem.Commands;

import de.MineX1991.ProxyBanSystem.API.BanManager;
import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class BanList
extends Command {
    public BanList() {
        super(ConfigManager.getBanlistCommand());
    }

    public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission("proxybansystem.banlist")) {
            if (BanManager.getBannedPlayers().size() == 0) {
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "§cDerzeit sind keine Spieler gebannt...");
                return;
            }
            sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "§6Folgende Spieler sind gebannt!");
            for (String x : BanManager.getBannedPlayers()) {
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a7c" + x + " \u00a78| \u00a77/check\u00a7c " + x);
            }
            sender.sendMessage("");
            sender.sendMessage(ConfigManager.getPrefix() + "§6Gebannte Spieler: §3" + BanManager.getBannedPlayers().size());
            
        } else {
            sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + ConfigManager.getNoPerm());
        }
    }
}

