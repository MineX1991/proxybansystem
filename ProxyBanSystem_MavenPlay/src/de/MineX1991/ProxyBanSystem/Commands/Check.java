package de.MineX1991.ProxyBanSystem.Commands;

import de.MineX1991.ProxyBanSystem.API.BanManager;
import de.MineX1991.ProxyBanSystem.API.MuteManager;
import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.plugin.Command;

public class Check
extends Command {
    public Check() {
        super(ConfigManager.getCheckCommand());
    }

    @SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
        if (sender.hasPermission("proxybansystem.check")) {
            if (args.length == 0) {
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Ausf\u00fchrung: \u00a7c/check <Spieler>");
            } else {
             
                if (BanManager.isBanned(args[0])) {
                	sender.sendMessage("§3§m---------------------------------");
                       sender.sendMessage("\u00a7eSpieler: \u00a7a" + args[0]);
                    sender.sendMessage("\u00a7eGebannt von: \u00a73" + BanManager.getWhoBanned(args[0]));
                    sender.sendMessage("\u00a7eGrund: \u00a73" + BanManager.getReason(args[0]));
                    sender.sendMessage("\u00a7eVerbleibende Zeit: \u00a73" + BanManager.getRemainingTime(args[0]));

                } else {
                    sender.sendMessage("");
                    sender.sendMessage("\u00a7cDer Spieler ist nicht gebannt!");
                	sender.sendMessage("§3§m---------------------------------");
                    sender.sendMessage("");
                }
                if (MuteManager.isMuted(args[0])) {
                       sender.sendMessage("\u00a7eSpieler: \u00a7a" + args[0]);
                    sender.sendMessage("\u00a7eGemuted von: \u00a73" + MuteManager.getWhoMuted(args[0]));
                    sender.sendMessage("\u00a7eGrund: \u00a73" + MuteManager.getReason(args[0]));
                    sender.sendMessage("\u00a7eVerbleibende Zeit: \u00a73" + MuteManager.getRemainingTime(args[0]));
                	sender.sendMessage("§3§m---------------------------------");

                } else {
                    sender.sendMessage("\u00a7cDer Spieler ist nicht gemuted!");
                }
            }
        } else {
            sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + ConfigManager.getNoPerm());
        }
    }
}

