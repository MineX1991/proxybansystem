package de.MineX1991.ProxyBanSystem.Commands;

import de.MineX1991.ProxyBanSystem.Manager.ConfigManager;
import java.util.Collection;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

public class Kick
extends Command {
    public Kick() {
        super(ConfigManager.getKickCommand());
    }

    @SuppressWarnings("deprecation")
	public void execute(CommandSender sender, String[] args) {
        ProxiedPlayer p = (ProxiedPlayer)sender;
        if (sender.hasPermission("proxybansystem.kick")) {
            if (args.length < 2) {
                sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Verwende \u00a7c/k <Spieler> <Grund>");
            } else {
                if (ProxyServer.getInstance().getPlayer(args[0]) == null) {
                    sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Dieser \u00a7cSpieler \u00a77ist nicht Online.");
                    return;
                }
                String message = "";
                int i = 1;
                while (i < args.length) {
                    message = String.valueOf(message) + args[i] + " ";
                    ++i;
                }
                sender.sendMessage("\u00a77Du hast den \u00a7cSpieler \u00a77erfolgreich vom Netzwerk geworfen.");
                ProxiedPlayer p2 = ProxyServer.getInstance().getPlayer(args[0]);
                for (ProxiedPlayer all : ProxyServer.getInstance().getPlayers()) {
                    if (!all.hasPermission("system.kick")) continue;
                    all.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a7c" + p2.getName() + "\u00a77 wurde von \u00a7c" + p.getName() + "\u00a77 von \u00a73ClayMC.net \u00a77gekickt.");
                    all.sendMessage(String.valueOf(ConfigManager.getPrefix()) + "\u00a77Grund: \u00a7a" + message);
                }
                String reason = message;
                p2.disconnect("\u00a77Du wurdest vom " + ConfigManager.getNetworkName() + " Netzwerk \u00a77gekickt. \n \u00a77Grund: \u00a7a" + reason);
            }
        } else {
            sender.sendMessage(String.valueOf(ConfigManager.getPrefix()) + ConfigManager.getNoPerm());
        }
    }
}

